Open notepad file readme.txt on full screen for better readibility

SpringBoot FriendManagementProject

Step 1: Run  maven project.
	After running two tables in your databases will get created automatically.
	
	a> Table name is "Account" - user account
	+----------+--------------+------+-----+---------+-------+
	| Field    | Type         | Null | Key | Default | Extra |
	+----------+--------------+------+-----+---------+-------+
	| id       | bigint(20)   | NO   | PRI | NULL    |       |
	| email    | varchar(255) | YES  |     | NULL    |       |
	| name     | varchar(255) | YES  |     | NULL    |       |
	| password | varchar(255) | YES  |     | NULL    |       |
	
	b> Table name is "friend_map" - maps one friend to another
	+--------+--------------+------+-----+---------+-------+
	| Field  | Type         | Null | Key | Default | Extra |
	+--------+--------------+------+-----+---------+-------+
	| email1 | varchar(255) | NO   | PRI | NULL    |       |
	| email2 | varchar(255) | NO   | PRI | NULL    |       |
	+--------+--------------+------+-----+---------+-------+
Step 2: Collections of Apis - All functionality provided in assignment has been added.
	
	@GetMapping(path="home")  - to get all users account all info.  Example: ec2-13-58-204-161.us-east-2.compute.amazonaws.com:8080/home
	@GetMapping(path="home/{email}") - to get account using email. Example: ec2-13-58-204-161.us-east-2.compute.amazonaws.com:8080/home/nitin.com
	@GetMapping(path="friend/list/{email}") -to get friends of user using email. Example:ec2-13-58-204-161.us-east-2.compute.amazonaws.com:8080/home/friend/list/nitin.com 
	- you cannot see password and id of your friends.
	@PostMapping(path="create") - to create new account. Example: ec2-13-58-204-161.us-east-2.compute.amazonaws.com:8080/home/create and pass json to it.
	json eg to create account:
	{
	"name":"nitin",
	"email":"nitin.com",
	"password": "abc"	
	}
	@PostMapping(path="friend/add")- add friend to user. Example: ec2-13-58-204-161.us-east-2.compute.amazonaws.com:8080/home/friend/add pass json to it. 
	json eg to add friend:
	{
	"email1":"nitin.com",
	"email2":"ritesh.com"
	}	
	@GetMapping(path="friend/{distance}/{email}")- get friends at specific distance. Example: ec2-13-58-204-161.us-east-2.compute.amazonaws.com/home/friend/2/nitin.com

Postman Apis Collections
	Get Requests:
		ec2-13-58-204-161.us-east-2.compute.amazonaws.com:8080/home
		ec2-13-58-204-161.us-east-2.compute.amazonaws.com:8080/home/nitin.com
		ec2-13-58-204-161.us-east-2.compute.amazonaws.com:8080/home/friend/list/nitin.com
		ec2-13-58-204-161.us-east-2.compute.amazonaws.com:8080/home/friend/2/nitin.com
	Post Requests:
		ec2-13-58-204-161.us-east-2.compute.amazonaws.com:8080/home/create
		ec2-13-58-204-161.us-east-2.compute.amazonaws.com:8080/home/friend/add