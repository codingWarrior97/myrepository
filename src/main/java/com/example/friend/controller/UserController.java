/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.friend.controller;

import com.example.friend.model.*;
import com.example.friend.dao.*;
import com.example.friend.service.Distance;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author Asus06
 */
@RestController
@RequestMapping("home")
public class UserController {

    @Autowired
    private UserRepo repo;
    @Autowired
    private FriendPairRepo repo2;

    @GetMapping
    public List<User> getAll() {
        return repo.findAllUserEmail();
    }

    @GetMapping(path = "/{email}")
    public User getUser(@PathVariable("email") String email) {
        return repo.findRecord(email);
    }

    @GetMapping(path = "friend/list/{email}")
    public List<User> getFriends(@PathVariable("email") String email) {
        return repo.findFriends(email);
    }

    @PostMapping(path = "create")
    public String addUser(@RequestBody User user) {

        try {
            User u = repo.save(user);
            return "added successfully " + u.toString();
        } catch (Exception e) {
            System.out.println("error");
        }
        return "Error,duplicate entry not allowed";
    }

    @PostMapping(path = "friend/add")
    public String addFriend(@RequestBody FriendPair fp) {
        User u1 = repo.findByEmail(fp.getEmail1());
        User u2 = repo.findByEmail(fp.getEmail2());
        if (u1 == null || u2 == null) {
            return "Create account first!!";
        }
        repo2.save(fp);
        repo2.save(new FriendPair(fp.getEmail2(), fp.getEmail1()));
        return "Succesful, you are now friends with each other " + fp.toString();
    }

    @GetMapping(path = "friend/{distance}/{email}")
    public List<String> getFriends(@PathVariable("distance") int k, @PathVariable("email") String email) {
        Distance d = new Distance();
        List<FriendPair> list = repo2.findAll();
        List<String> sl = d.getFriends(list, new ArrayList<>(), "", 1, email, k);
        return sl;
    }

}
