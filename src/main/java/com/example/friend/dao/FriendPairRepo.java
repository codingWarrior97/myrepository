package com.example.friend.dao;

import com.example.friend.model.*;
import org.springframework.data.jpa.repository.JpaRepository;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Asus06
 */
public interface FriendPairRepo extends JpaRepository<FriendPair, CompositeKey> {

}
