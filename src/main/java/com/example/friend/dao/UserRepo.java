/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.friend.dao;

import com.example.friend.model.User;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author Asus06
 */
public interface UserRepo extends JpaRepository<User, Integer> {

    public User findByEmail(String email);

    @Query("select new com.example.friend.model.User(u.name,u.email) from User u where u.email in ("
            + "select p.email2 from FriendPair p where p.email1 = ?1"
            + ")")
    public List<User> findFriends(String email);

    @Query("select new com.example.friend.model.User(u.name,u.email) from User u")
    public List<User> findAllUserEmail();

    @Query("select new com.example.friend.model.User(u.id,u.name,u.email) from User u where u.email = ?1")
    public User findRecord(String email);
}
