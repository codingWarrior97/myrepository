/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.friend.model;

import lombok.*;

/**
 *
 * @author Asus06
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class FriendDetail {

    private User user;
    private FriendPair fp;
}
