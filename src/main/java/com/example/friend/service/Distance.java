/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.friend.service;

import java.util.List;
import com.example.friend.model.*;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Asus06
 */
public class Distance {

    public synchronized static List<String> getFriends(List<FriendPair> list, List<String> temp, String ntd, int cl, String email, int k) {

        if (cl > k) {
            return new ArrayList<>();
        }
        try {
            for (FriendPair u : list) {

                if (u.getEmail1().equals(email)) {
                    if (cl == k && u.getEmail2() != ntd && !temp.contains(u.getEmail2())) {
                        temp.add(u.getEmail2());
                    }
                    getFriends(
                            list,
                            temp,
                            email,
                            cl + 1,
                            u.getEmail2(),
                            k);
                    temp.remove(email);
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return temp;
    }
}
